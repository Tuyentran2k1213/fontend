function changedk(tagname) {
    var dki = document.getElementById(tagname);
    var dnhap = document.getElementById("dn");
    var changecolor = document.getElementById("dangki");
    var stillcolor = document.getElementById("dangnhap");  
    dki.style.display = "block";
    dnhap.style.display = "none";
    changecolor.style.backgroundColor = "rgb(222, 213, 135)";
    stillcolor.style.backgroundColor = "rgb(98, 149, 153)";
}
function changedn(tagname) {
    var dnhap = document.getElementById(tagname);
    var dki = document.getElementById("dk");
    var changecolor = document.getElementById("dangnhap");
    var stillcolor = document.getElementById("dangki");
    dnhap.style.display = "block";
    dki.style.display = "none";
    changecolor.style.backgroundColor = "rgb(222, 213, 135)";
    stillcolor.style.backgroundColor = "rgb(98, 149, 153)";
}
function rewrite(txt){
    var warn = document.getElementById("notice");
    warn.style.display = "block";
    warn.innerHTML = txt;
}
function checktendangnhap() {
        var warn = document.getElementById("notice");
        var name = document.getElementById("namelog");
        var minlength = 8;
        var maxlength = 12;
        if(name.value == ""){
                    rewrite("vui lòng nhập hết tất cả các phần trống");    
                    return false;        
                }
                else if(name.value.length > maxlength || name.value.length < minlength){
                   rewrite("vui lòng nhập user name from " + minlength + " to " + maxlength + " characters");
                   return false;
                }
                else{
                    warn.style.display = "none";
                    return true;
                }
            }
function checkmaildangnhap() {
    var warn = document.getElementById("notice");
    var name = document.getElementById("maillog");
    var letters = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+([a-zA-Z0-9]{2,4})?$/;
    if(name.value == ""){
        rewrite("vui lòng nhập hết tất cả các phần trống");
        return false;     
         }
        else if(name.value.match(letters)){
            warn.style.display = "none";
            return true;
        }
        else{
            rewrite("please type your correct email");
            return false;
        }
    }
function checkpassdangnhap() {
    var warn = document.getElementById("notice");
    var name = document.getElementById("passlog");
    var letters = /^[a-zA-Z0-9_\.\-\@]+$/;
    if(name.value == ""){
        rewrite("vui lòng nhập hết tất cả các phần trống");            
        }
        else if(name.value.match(letters)){
            warn.style.display = "none";
            return true;
        }
        else{
            rewrite("please your password should be character A to z");
            return false;
        }
    }
function checktuoidangnhap() {
    var warn = document.getElementById("notice");
    var name = document.getElementById("birthlog");
    var minage = 18;
    var maxage = 30;
    if(name.value == ""){
        rewrite("vui lòng nhập hết tất cả các phần trống"); 
        return false;        
        }
        else if(isNaN(name.value)){
            rewrite("your age must be a number");
            return false;
        }
        else if(parseInt(name.value) > maxage || parseInt(name.value) < minage){
            rewrite("your age just only from " + minage + " to " + maxage + " age to sign in");
            return false;
        }
        else{
            warn.style.display = "none";
            return true;
        }
    }
function checknghenghiep() {
    var warn = document.getElementById("notice");
    var name = document.getElementById("work");
    if(name.selectedIndex == 0){
        warn.style.display = "block";
        warn.innerHTML += "<br/>you must choose your work";
        return false;
    }
    else{
        warn.style.display = "none";
        return true;;
    }
}
function sexual(){
    var nam = document.getElementById("male");
    var nu = document.getElementById("female");
    if(!nam.checked && !nu.checked){
        rewrite("you must choose your sex");
        return false;
    }
    else{
        warn.style.display = "none";
        return true;
    }
}
function forsubmit() {
    return checktendangnhap() && checkmaildangnhap() && checkpassdangnhap() && checktuoidangnhap() && checknghenghiep() && sexual();
}